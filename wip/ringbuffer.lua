local class = require "lib.middleclass"
local log = require "lib.log"
local inspect = require "lib.inspect"

local RingBuffer = class("RingBuffer")

function wrap(idx, max) return (idx - 1) % max + 1 end

function RingBuffer:initialize(size)
    self.head = 1
    self.tail = 1
    self.size = size
end

function RingBuffer:isFull()
    return self.head - self.tail >= self.size
end

function RingBuffer:isEmpty()
    return self.head == self.tail
end

function RingBuffer:push(value)
    if self:isFull() then -- silently override oldest element
        self.tail = self.tail + 1
    end

    local index = wrap(self.head, self.size)
    self[index] = value
    self.head = self.head + 1
end

function RingBuffer:pop()
    if self:isEmpty() then
        error("Cannot pop from an empty RingBuffer.")
    end

    local index = wrap(self.tail, self.size)
    local value = self[index]
    self[index] = nil
    self.tail = self.tail + 1
    return value
end

function RingBuffer:peek()
    local index = wrap(self.tail, self.size)
    return self[index]
end


-- simple test
local d = RingBuffer(5)
d:push("first")
d:push("second")
d:push("third")
d:push("fourth")
d:push("fifth")
d:push("sixth")


d:pop()
