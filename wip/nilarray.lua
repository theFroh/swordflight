local inspect = require "lib.inspect"
local log = require "lib.log"

function insert(array, free, element)
    local idx = #array + 1
    if #free > 0 then
        idx = table.remove(free)
    end

    array[idx] = element
    return idx
end

function remove(array, free, idx)
    table.insert(free, idx, 1)
    array[idx] = nil
end

local free = {}
local t = {}

insert(t, free, "value")
insert(t, free, "what")
insert(t, free, "no")
insert(t, free, "ok")
insert(t, free, "cool")
insert(t, free, "bye")

log.debug(#t)
remove(t, free, 3)
log.debug(#t)
remove(t, free, 6)
log.debug(#t)

for k,v in ipairs(t) do
    log.debug(k,v)
end
