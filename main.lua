local Gamestate = require "lib.hump.gamestate"
local Timer = require "lib.hump.timer"
local log = require "lib.log"
local fonts = require "fonts"

-- gamestates
-- TODO: Move gamestates into a folder?
local menu = require "menu"

function love.run()
    if love.math then
        love.math.setRandomSeed(os.time())
    end

    if love.load then love.load(arg) end

    -- We don't want the first frame's dt to include time taken by love.load.
    if love.timer then love.timer.step() end

    local dt = 0

    local acc = 0
    local fixed_dt = 1/60

    -- Main loop time.
    while true do
        -- Process events.
        if love.event then
            love.event.pump()
            for name, a,b,c,d,e,f in love.event.poll() do
                if name == "quit" then
                    if not love.quit or not love.quit() then
                        return a
                    end
                end
                love.handlers[name](a,b,c,d,e,f)
            end
        end

        -- Update dt, as we'll be passing it to update
        if love.timer then
            love.timer.step()
            dt = love.timer.getDelta()
        end

        -- Call update and draw
        if love.update then love.update(dt) end -- will pass 0 if love.timer is disabled

        if love.fixedUpdate then
            acc = acc + dt
            while acc > fixed_dt do
                love.fixedUpdate(fixed_dt)
                acc = acc - fixed_dt
            end
        end

        if love.graphics and love.graphics.isActive() then
            love.graphics.clear(love.graphics.getBackgroundColor())
            love.graphics.origin()
            if love.draw then love.draw() end
            love.graphics.present()
        end

        if love.timer then love.timer.sleep(0.001) end
    end
end

function love.load()
    fonts.load()

    Gamestate.registerEvents()
    Gamestate.registerEvents{"fixedUpdate"}
    Gamestate.switch(menu)

    love.graphics.setFont(fonts.text)
    -- log.outfile = "log.log"-
end

function love.draw()

end

function love.update(dt)
    Timer.update(dt)
end

function love.focus(f)
    -- if f then
    --     love.audio.setVolume(1)
    -- else
    --     love.audio.setVolume(0)
    -- end
end

function love.keyreleased(k)
    if k == "escape" or k == "q" then
        love.event.quit()
    elseif k == "m" then
        if love.audio.getVolume() > 0 then
            love.audio.setVolume(0)
        else
            love.audio.setVolume(1)
        end
    end
end
