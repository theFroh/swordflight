local enet = require "enet"
local Class = require "lib.middleclass"
local Signals = require "lib.hump.signal"
local MsgPack = require "lib.MessagePack"
local inspect = require "lib.inspect"
local log = require "lib.log"

local NetServer = Class("NetServer")

function NetServer:initialize(addr, port)
    self.time = 0

    self.recv = 0
    self.sent = 0
    self.recv_d = 0
    self.sent_d = 0

    log.info('[SERVER] Hosting on *:' .. port)
    self.host = enet.host_create('*:' .. port, 64, 2)
    self.host:compress_with_range_coder(true)

    self.clients = {}
end

function NetServer:update(dt)
    self.sent_d = self.sent_d + .005*(((self.host:total_sent_data() - self.sent)/dt) - self.sent_d)
    self.recv_d = self.recv_d + .005*(((self.host:total_received_data() - self.recv)/dt) - self.recv_d)

    self.sent = self.host:total_sent_data()
    self.recv = self.host:total_received_data()

    local event = self.host:service()
    while event do
        local uid = tostring(event.peer:index())

        if event.type == "receive" then
            local ok,msg = pcall(love.math.decompress, event.data, "lz4")
            if not ok then
                log.error(msg)
            else
                ok,msg = pcall(MsgPack.unpack, msg)
                if not ok then
                    log.error(msg)
                else
                    if msg.type == "message" then
                        Signals.emit("server-message", event.peer, msg.from, msg.data)
                    end
                end
            end
        elseif event.type == "disconnect" then
            log.info('[SERVER] Client disconnected')

            -- remove client
            self.clients[uid] = nil

            -- notify clients of disconnected peer
            self:broadcast(uid, "disconnected")
            Signals.emit("server-disconnected", uid)
        elseif event.type == "connect" then
            log.info('[SERVER] Client connected, assigned UID of', uid)

            -- add client
            self.clients[uid] = event.peer

            -- notify new client of uid
            self:send(event.peer, uid, "greeting")

            -- notify all clients of new client
            self:broadcast(uid, "connected")
            Signals.emit("server-connected", uid)
        end

        event = self.host:service()
    end
end

function NetServer:send(peer, message, type, channel, flag)
    channel = channel or 0
    flag = flag or "reliable"

    local msg = {
        uid  = peer:index(),
        time = self.time,
        type = type or "message",
        data = message
    }

    local msgstr = MsgPack.pack(msg)
    local data = love.math.compress(msgstr, "lz4")
    peer:send(data:getString(), channel, flag)
end

function NetServer:broadcast(message, type, channel, flag)
    for k,v in pairs(self.clients) do
        self:send(v, message, type, channel, flag)
    end
end

return NetServer
