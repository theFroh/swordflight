local enet = require "enet"
local Class = require "lib.middleclass"
local Signals = require "lib.hump.signal"
local MsgPack = require "lib.MessagePack"
local inspect = require "lib.inspect"
local log = require "lib.log"

local NetClient = Class("NetClient")

-- TODO: Delta compression, again

function NetClient:initialize(addr, port)
    self.time = 0
    self.ping = math.huge
    self.uid = nil
    self.recv = 0
    self.sent = 0
    self.recv_d = 0
    self.sent_d = 0

    self.connected = false

    log.info('[CLIENT] Joining ' .. addr)
    self.host = enet.host_create(nil, 1, 2)
    self.host:compress_with_range_coder(true)
    self.server = self.host:connect(addr .. ':' .. port, 2)
    self.server:ping_interval(100)
end

function NetClient:update(dt)
    self.sent_d = self.sent_d + .005*(((self.host:total_sent_data() - self.sent)/dt) - self.sent_d)
    self.recv_d = self.recv_d + .005*(((self.host:total_received_data() - self.recv)/dt) - self.recv_d)

    self.sent = self.host:total_sent_data()
    self.recv = self.host:total_received_data()

    self.ping = self.server:round_trip_time()
    local event = self.host:service()
    while event do
        if event.type == "receive" then
            local ok,msg = pcall(love.math.decompress, event.data, "lz4")
            if not ok then
                log.error(msg)
            else
                ok,msg = pcall(MsgPack.unpack, msg)
                if not ok then
                    log.error(msg)
                else
                    -- sync clocks
                    self.time = msg.time + self.ping/2000
                    if msg.type == "message" then
                        Signals.emit("client-message", msg.data)
                    elseif msg.type == "disconnected" then
                        log.info("[CLIENT] DISCO")
                        Signals.emit("client-disconnected", msg.data)
                    elseif msg.type == "greeting" then
                        self.uid = msg.data
                        self.connected = true
                        log.info("[CLIENT] Assigned UID of", msg.data)
                        Signals.emit("client-greeting", msg.data)
                    elseif msg.type == "connected" then
                        Signals.emit("client-connected", msg.data)
                    end
                end
            end
        elseif event.type == "connected" then
            log.info("[CLIENT] Connected", event.data)
        end

        event = self.host:service()
    end
end

function NetClient:sendMessage(message, channel, reliable)
    reliable = reliable or "reliable"
    channel = channel or 0

    local msg = {
        type = "message",
        from = self.uid,
        data = message
    }

    local msgstr = MsgPack.pack(msg)
    local data = love.math.compress(msgstr, "lz4")

    self.server:send(data:getString(), channel, reliable)
end

return NetClient
