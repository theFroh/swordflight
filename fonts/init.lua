local fonts = {}
local tPx, fPx = love.window.toPixels, love.window.fromPixels

function fonts.load()
    fonts.title = love.graphics.newFont("fonts/merriweather/Merriweather-Regular.ttf", tPx(100))
    fonts.text = love.graphics.newFont("fonts/open sans/OpenSans-Regular.ttf", tPx(16))
    fonts.tinytext = love.graphics.newFont("fonts/open sans/OpenSans-Regular.ttf", tPx(8))
    fonts.bold_text = love.graphics.newFont("fonts/open sans/OpenSans-Bold.ttf", tPx(16))
end

return fonts
