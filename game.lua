local game = {} -- gamestate
local Gamestate = require "lib.hump.gamestate"
local previous

local log = require "lib.log"
local inspect = require "lib.inspect"
local debugGraph = require "lib.debugGraph"
local suit = require "lib.suit"
local Signals = require "lib.hump.signal"
local Vector = require "lib.hump.vector"
local const = require "game.const"

local fonts = require "fonts"

local NetServer = require "net.netserver"
local NetClient = require "net.netclient"
local ClientWorld = require "game.clientworld"
local ServerWorld = require "game.serverworld"
local SimWorld = require "game.simworld"
local Ship = require "game.ship"

local draw_server = false
local draw_sim = false
local fps_graph, mem_graph

local tPx, fPx = love.window.toPixels, love.window.fromPixels

function game:keyreleased(key)
    if key == "o" then
        draw_server = not draw_server
    elseif key == "r" then
        draw_sim = not draw_sim
    elseif key == "y" then
        self.client.server:disconnect_now()
        self.client = NetClient(self.address, const.PORT, const.CLIENT_TICKRATE)
        self.client_world:destroy()
        self.client_world = ClientWorld("game/maps/obj/empty.msgpack", uid)
    end
end

function game:init()
    fps_graph = debugGraph:new('fps', 0, 0, tPx(50), tPx(30), 0.5, nil, fonts.text)
    mem_graph = debugGraph:new('mem', 0, tPx(30), tPx(50), tPx(30), 0.5, nil, fonts.text)
    rtt_graph = debugGraph:new('custom', 0, tPx(60), tPx(50), tPx(30), 0.5, nil, fonts.text)
end

function game:enter(previous, username, address, hosting)
    self.previous = previous

    self.username = username

    self.showMenu = false
    self.tick = 0

    self.client = nil
    self.server = nil

    self.client_world = nil
    self.server_world = nil

    log.debug("Attempting to establish connections")

    self.hosting = hosting

    if hosting then
        love.window.setTitle("Swordflight Host")
        self.server = NetServer(address, const.PORT, const.SERVER_TICKRATE)
        address = "localhost"

        self.server_world = ServerWorld("game/maps/obj/empty.msgpack")
        self.last_input_tick = {}
        self.client_history = {}
    end

    self.address = address
    self.client = NetClient(address, const.PORT, const.CLIENT_TICKRATE)

    -- register callbacks, yay closures
    -- Signals.register("server-tick", function(...) self:server_tick(...) end)
    Signals.register("server-message", function(...) self:server_message(...) end)
    Signals.register("server-connected", function(...) self:server_connected(...) end)
    Signals.register("server-disconnected", function(...) self:server_disconnected(...) end)

    -- Signals.register("client-tick", function(...) self:client_tick(...) end)
    Signals.register("client-message", function(...) self:client_message(...) end)
    Signals.register("client-greeting", function(...) self:client_greeting(...) end)
    Signals.register("client-connected", function(...) self:client_connected(...) end)
    Signals.register("client-disconnected", function(...) self:client_disconnected(...) end)
end

function game:getUserInput()
    local move = Vector((love.keyboard.isDown("a", "left") and -1 or 0) +
        (love.keyboard.isDown("d", "right") and 1 or 0),
        (love.keyboard.isDown("w", "up") and -1 or 0) +
        (love.keyboard.isDown("s", "down") and 1 or 0)
    )

    move:trimInplace(1)

    local mouse
    if self.client and self.client_world then
        mouse = Vector(self.client_world.player_cam.cam:worldCoords(love.mouse.getPosition()))
    else
        mouse = Vector(love.mouse.getPosition())
    end

    return {
        move = {
            x = move.x,
            y = move.y
        },

        aim = {
            x = mouse.x,
            y = mouse.y
        },
        action = love.mouse.isDown(1)
    }
end

function game:fixedUpdate(dt)
    if self.client and (self.tick % const.CLIENT_TICKRATE == 0) then
        self:client_tick()
    end
    if self.server and (self.tick % const.SERVER_TICKRATE == 0) then
        self:server_tick()
    end

    if self.client_world and self.client.connected then
        local input = self:getUserInput()
        self.client:sendMessage({type = "input", input = input, tick = self.tick}, 0, "unsequenced")
    end

    self.tick = self.tick + 1
end

function game:update(dt)
    if self.client then
        self.client:update(dt)
    end

    if self.server then
        self.server:update(dt)
    end

    if self.server_world then
        self.server_world:update(dt)
    end

    if self.client_world and self.client.connected then
        self.client_world:update(dt)
    end

    if self.showMenu then
        suit.layout:reset(tPx(love.graphics.getWidth() / 2 - 250 / 2, 0), tPx(10, 10))
        suit.Label("Select a class to spawn with:", {align = "left", font = fonts.text}, suit.layout:row(tPx(250, 20)))
        local sword = suit.Button("Sword", {font = fonts.text}, suit.layout:row(nil, tPx(40)))
        local hammer = suit.Button("Hammer", {font = fonts.text}, suit.layout:row())
        if sword.hit then
            self.showMenu = false
            self.client:sendMessage({type = "preference_change", preference = "sword"}, 1, "reliable")
        end
    end

    fps_graph:update(dt)
    mem_graph:update(dt)
    rtt_graph:update(dt, math.floor(self.client.ping))
    rtt_graph.label = string.format("Ping (ms) %d", self.client.ping)
end

function game:draw()
    if self.server_world and draw_server then -- server underlay
        self.server_world:draw()
        love.graphics.setColor(0, 0, 0, 0.95*255)
        love.graphics.rectangle("fill", 0,0, love.graphics.getWidth(), love.graphics.getHeight())
        love.graphics.setColor(255,255,255,255)
    end

    if self.client_world then
        self.client_world:draw()
    end

    suit:draw()
    love.graphics.setColor(255,255,255,150)
    fps_graph:draw()
    mem_graph:draw()
    rtt_graph:draw()
    love.graphics.setColor(255,255,255,255)
end

-- SERVER NETWORKING CALLBACKS
function game:server_tick()
    if self.server_world then
        local worldstate = self.server_world:getState()
        for uid,peer in pairs(self.server.clients) do
            local delta, state = self.server_world:getDeltaState(self.client_history[uid])
            self.server:send(peer, {type = "worldstate", state = delta, tick = self.tick}, nil, 0, "reliable")
            self.client_history[uid] = state -- TODO: last received input tick?
        end
    end
end

function game:server_message(peer, from, data)
    -- log.debug("Got message", peer, from, inspect(data))

    if data.type == "preference_change" and data.preference then
        self.server_world:setReady(from, data.preference)
    elseif data.type == "greeting" and data.name then
        self.server_world:addPlayer(from, data.name)

        local delta, state = self.server_world:getDeltaState()
        self.server:send(peer, {type = "worldstate", state = delta, tick = self.tick}, nil, 0, "reliable")
        self.client_history[from] = state -- TODO: last received input tick?


    elseif data.type == "input" and data.input then
        self.last_input_tick[from] = data.tick
        self.server_world:sendInput(from, data.input)
    else
        log.error("Unknown message type!", inspect(data))
    end
end

function game:server_connected(uid)

end

function game:server_disconnected(uid)
    if self.server_world then
        self.server_world:removePlayer(uid)
        self.client_history[uid] = nil
    end
end

-- CLIENT NETWORKING CALLBACKS

function game:client_tick()

end

function game:client_message(data)
    if data.type == "worldstate" and data.state then
        if self.client_world then
            self.client_world:applyDeltaState(data.state)
        end
    elseif data.type == "shutdown" then
        self.client_world:destroy()
        self.client_world = nil
        Gamestate.switch(self.previous)
    else
        log.error("Unknown message type!", inspect(data))
    end
end

function game:client_greeting(uid)
    self.client:sendMessage({type = "greeting", name = self.username}, 1, "reliable")

    log.debug("Creating client world")
    self.client_world = ClientWorld("game/maps/obj/empty.msgpack", uid)

    self.showMenu = true
end

function game:client_connected(data)

end

function game:client_disconnected(data)

end

function game:quit()
    if self.client then
        self.client.server:disconnect_now()
    end

    if self.server then
        self.server:broadcast({type = "shutdown"})
        self.server.host:flush()
        -- idk man

        -- for _,client in pairs(server.clients) do
        --     client:disconnect_later()
        -- end
    end
    return false
end

return game
