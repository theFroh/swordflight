return {
    -- GRAVITY = 0,
    -- GRAVITY = 9.81,
    GRAVITY = 9.81,
    METER = 100,
    MAXVEL = 700,
    TIGHTNESS = 0.2,

    PORT = 12345,

    -- NOTE: these are periods; i.e. every x ticks (lower is faster)
    SERVER_TICKRATE = 4,
    CLIENT_TICKRATE = 1,

    PLAYER_RADIUS = 10
}
