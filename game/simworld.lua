local log = require "lib.log"
local inspect = require "lib.inspect"
local class = require "lib.middleclass"

local Vector = require "lib.hump.vector"
local const = require "game.const"
local World = require "game.world"
local Ship = require "game.ship"

local Sword = require "game.weapons.sword"

local SimWorld = class("SimWorld", World)

function SimWorld:initialize(local_uid)
    World.initialize(self, true, local_uid)
end

function SimWorld:sendInput(uid, input)
    World.sendInput(self, uid, input)
end

-- expects an ordered list of local player inputs
function SimWorld:simulate(inputs)
    -- TODO: call World.update(self, dt) as needed
end

function SimWorld:update(dt)
    World.update(self, dt)
end

function SimWorld:setState(state)
    World.setState(self, state, true) -- snap
end

function SimWorld:getState()
    return World.getState(self, state)
end

function SimWorld:draw()
    World.draw(self)
end

return SimWorld
