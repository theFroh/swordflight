local log = require "lib.log"
local inspect = require "lib.inspect"
local class = require "lib.middleclass"

local Vector = require "lib.hump.vector"
local const = require "game.const"

local World = require "game.world"
local Player = require "game.player"
local Ship = require "game.ship"
local Sword = require "game.weapons.sword"

local ServerWorld = class("ServerWorld", World)

function ServerWorld:initialize(map_path)
    World.initialize(self, map_path, false, true)
    assert(self.is_authority)
end

function ServerWorld:setReady(uid, preference)
    preference = preference or "Sword" -- TODO: default pref
    local player = self:getPlayer(uid)
    if player and player.ready_state == "select" or player.ready_state == "respawn" then
        player.ready_state = "ingame"

        local spawn_pos = self.map.spawn_points[love.math.random(#self.map.spawn_points)]
        local new_ship = Ship(self, spawn_pos.x, spawn_pos.y, player)
        -- local new_ship = Ship(self, love.math.random(0, love.graphics.getWidth()), love.math.random(0, love.graphics.getHeight()), player)
        player.ship = new_ship
        player.ship_eid = new_ship.eid
        -- TODO: weapon types
        local ship_weapon = Sword(self, new_ship)
        new_ship:equipWeapon(ship_weapon)
    end
end

-- TODO: Spawn invulnerability (and inability to attack), and respawn times

function ServerWorld:addPlayer(uid, name)
    if self.players[uid] then
        log.error("Attempted to add an already existing player!")
        return
    end

    Player(self, uid, name)
end

function ServerWorld:removePlayer(uid)
    if not self.players[uid] then
        log.error("Attempted to remove a non-existant player!")
        return
    end

    self:getPlayer(uid):destroy()
end

function ServerWorld:register(entity)
    log.info("Spawning entity", entity, self.highest_eid)
    local eid = self.highest_eid
    self.entities[eid] = entity
    self.highest_eid = self.highest_eid + 1

    return eid
end

function ServerWorld:deregister(eid)
    log.info("Removing entity", eid)
    self.entities[eid] = false
end

function ServerWorld:sendInput(uid, input)
    World.sendInput(self, uid, input)
end

function ServerWorld:update(dt)
    World.update(self, dt)
end

-- function ServerWorld:setState(state)
--     World.setState(self, state, false) -- snap
-- end

function ServerWorld:getState()
    return World.getState(self, state)
end

function ServerWorld:draw()
    World.draw(self)
end

function ServerWorld:destroy()
    World.destroy(self)
end

return ServerWorld
