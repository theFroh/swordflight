local log = require "lib.log"
local inspect = require "lib.inspect"
local class = require "lib.middleclass"

local Vector = require "lib.hump.vector"
local fonts = require "fonts"
local const = require "game.const"
local Map = require "game.map"
local Ship = require "game.ship"
local Player = require "game.player"
local LocalPlayer = require "game.localplayer"

local Sword = require "game.weapons.sword"

local World = class("World")

-- TODO: Disable sounds/effects rendering, for server + sim world? how

function World:initialize(map_path, is_fancy, is_authority, local_uid)
    self.is_fancy = is_fancy or false
    self.is_authority = is_authority or false
    self.local_uid = local_uid

    love.physics.setMeter(const.METER)
    self.physworld = love.physics.newWorld(0, const.GRAVITY*const.METER)
    love.audio.setDopplerScale(7/const.METER)

    self.physworld:setCallbacks(nil, nil, World.static.preSolve, World.static.postSolve)

    self.players = {}
    self.entities = {}
    self.highest_eid = 1

    -- load map
    self.map = Map(self, map_path)

    self.tick = 0
    self.first_state = true

    self.kill_feed = {}
end

function World:addKill(killer, victim)
    if killer then
        killer.kills = killer.kills + 1
    end
    victim.deaths = victim.deaths + 1
end

function World.static.postSolve(a, b, coll, normalimpulse1, tangentimpulse1, normalimpulse2, tangentimpulse2)
    local a_body, b_body = a:getBody(), b:getBody()
    local a_user = a_body:getUserData()
    local b_user = b_body:getUserData()

    if a_user and a_user.handlePostSolve then
        a_user:handlePostSolve(a, b, coll, normalimpulse1, tangentimpulse1, normalimpulse2, tangentimpulse2)
    end

    if b_user and b_user.handlePostSolve then
        b_user:handlePostSolve(b, a, coll, normalimpulse1, tangentimpulse1, normalimpulse2, tangentimpulse2)
    end
end

function World.static.preSolve(a, b, coll)
    local a_body, b_body = a:getBody(), b:getBody()
    local a_user = a_body:getUserData()
    local b_user = b_body:getUserData()
    if a_user and a_user.handlePreSolve then
        a_user:handlePreSolve(a, b, coll)
    end

    if b_user and b_user.handlePreSolve then
        b_user:handlePreSolve(b, a, coll)
    end
end

function World:sendInput(uid, input)
    local player = self:getPlayer(uid)
    if player and player.ready_state == "ingame" then
        local eid = player.ship_eid

        if self.entities[eid] then
            self.entities[eid]:setInput(input)
        end
    end
end

function World:getPlayer(uid)
    local player = nil
    local player_eid = self.players[uid]
    if player_eid then
        player = self.entities[player_eid]
        if not player then
            log.debug("Can't find player ent with", uid, player_eid)
        end
    end

    return player
end

function World:update(dt)
    -- log.debug(inspect(self.entities, {depth=2}))
    for eid = 1, self.highest_eid do
        local entity = self.entities[eid]
        if entity and entity.update and not entity.parent then
            entity:update(dt)
        end
    end

    self.physworld:update(dt)
    self.tick = self.tick + 1
end

function World:draw()
    self.map:draw()

    for eid = 1, self.highest_eid do
        local entity = self.entities[eid]
        if entity and entity.draw and not entity.parent then
            entity:draw()
        end
    end
end

function World:getState()
    local state = {
        tick = self.tick,
        entities = {}
    }

    for eid = 1, self.highest_eid do
        local entity = self.entities[eid]
        if entity then
            state.entities[eid] = entity:getState()
        else
            state.entities[eid] = false -- marked for removal?
        end
    end

    state.players = self.players
    state.highest_eid = self.highest_eid
    state.kill_feed = self.kill_feed

    return state
end

function World:getDeltaState(last_full_state)
    local delta = {
        add = {},
        del = {},
        mod = {}
    }

    local new_full_state = self:getState()

    for eid = 1, self.highest_eid do
        local last_entity_state = last_full_state and last_full_state.entities[eid] or nil
        local new_entity_state = new_full_state.entities[eid]

        if last_entity_state and new_entity_state then
            table.insert(delta.mod, {eid = eid, state = new_entity_state})
        elseif not last_entity_state and new_entity_state then
            table.insert(delta.add, {eid = eid, state = new_entity_state})
        elseif last_entity_state and not new_entity_state then
            table.insert(delta.del, {eid = eid})
            log.debug("Marking", eid, "for removal")
        end -- else, it has been removed and this is known

    end

    delta.players = self.players
    delta.kill_feed = self.kill_feed
    delta.highest_eid = self.highest_eid

    return delta, new_full_state
end

function World:applyDeltaState(delta_state)
    self.players = delta_state.players
    self.kill_feed = delta_state.kill_feed
    self.highest_eid = delta_state.highest_eid

    for _, ent_info in ipairs(delta_state.add) do
        local eid, estate = ent_info.eid, ent_info.state
        if self.entities[ent_info.eid] then
            log.error("Attempting to add an entity which already exists!")
        else
            if estate.class == "Ship" then
                local parent_eid = estate.parent_eid
                if self.entities[parent_eid] then
                    local parent = self.entities[parent_eid]
                    local new_ship = Ship(self, estate.pos.x, estate.pos.y, parent, estate)
                    parent.ship = new_ship
                    log.debug("Creating Ship on client", new_ship.eid)
                else
                    Ship(self, estate.pos.x, estate.pos.y, nil, estate)
                    log.debug("Creating Ship on client")
                end

            elseif estate.class == "Sword" then
                local parent_eid = estate.parent_eid
                if self.entities[parent_eid] then
                    local parent = self.entities[parent_eid]
                    local weapon = Sword(self, parent, estate)
                    parent:equipWeapon(weapon)
                    log.debug("Creating Sword on client", weapon.eid)
                else
                    log.error("Attempt to construct a missing weapon before its parent exists!")
                end
            elseif estate.class == "Player" then
                local new_player
                if self.local_uid and estate.uid == self.local_uid then
                    new_player = LocalPlayer(self, estate.uid, estate.name, estate)
                    log.debug("Upcoming LocalPlayer!")
                else
                    new_player = Player(self, estate.uid, estate.name, estate)
                end
                log.debug("Creating Player on client", new_player.eid)
            else
                log.error("Unhandled entity creation")
            end
        end
    end

    for _, ent_info in ipairs(delta_state.mod) do
        local eid, estate = ent_info.eid, ent_info.state
        local entity = self.entities[eid]
        if not estate.parent_eid then -- if not managed by a parent (is child)
            entity:setState(estate, snap)
        end
    end

    -- log.error(inspect(delta_state.del))
    for _, ent_info in ipairs(delta_state.del) do
        local eid = ent_info.eid
        local entity = self.entities[eid]

        if not entity then
            log.error("Attempting to remove a non-existant entity")
        else
            log.error(eid, entity, entity.parent ~= nil)
            -- if not entity.parent then -- if not managed by a parent (is child)
                log.warn("Delta removing entity", eid)
                entity:destroy()
            -- end
        end
    end
end

function World:destroy()
    for eid = 1, self.highest_eid do
        local entity = self.entities[eid]
        if entity and entity.destroy and not entity.parent then
            entity:destroy()
        end
    end
    self.physworld:destroy()
end

return World
