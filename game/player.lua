local log = require "lib.log"
local inspect = require "lib.inspect"
local class = require "lib.middleclass"

local const = require "game.const"

local Player = class("Player")

function Player:initialize(world, uid, name, client_state)
    self.uid = uid
    self.name = name
    self.ready_state = "select"
    self.respawn_time = 0

    self.kills = 0
    self.deaths = 0

    self.ship_eid = nil -- no ship by default
    self.ship = nil
    self.preference = "Sword"
    self.world = world

    if world.is_authority then
        self.eid = self.world:register(self)
        self.world.players[self.uid] = self.eid
    else
        assert(client_state, "Missing required state for initialization on client")
        self.eid = client_state.eid
        self.world.entities[self.eid] = self
    end
end

function Player:destroy()
    if self.ship then
        self.ship:destroy()
    end

    if self.world.is_authority then
        self.world:deregister(self.eid)
        self.world.players[self.uid] = nil
    else
        self.world.entities[self.eid] = false
        self.world.players[self.uid] = nil
    end
end

function Player:draw()
    if self.ship then
        love.graphics.printf(self.name, self.ship.body:getX(), self.ship.body:getY() - 40, 200, "center", 0, 1, 1, 100, 0)
        self.ship:draw()
    end
end

function Player:shipDied()
    log.debug("Ship died")
    self.ready_state = "respawn"
    self.respawn_time = 2

    self.world:addKill(self.ship.last_dealer, self)

    self.ship = nil
    self.ship_eid = nil
end

function Player:update(dt)
    if self.ship then
        self.ship:update(dt)
    end

    if self.respawn_time > 0 then
        self.respawn_time = math.max(0,self.respawn_time - dt)
        if self.world.is_authority and self.respawn_time == 0 then
            self.world:setReady(self.uid, "Sword")
            log.debug("Spawning new ship for character?")
        end
    end
end

function Player:getState()
    local state = {
        class = "Player",
        eid = self.eid,
        uid = self.uid,
        name = self.name,
        ship_eid = self.ship and self.ship.eid or nil,
        ready_state = self.ready_state,
        respawn_time = self.respawn_time,
        preference = self.prefrence,
        kills = self.kills,
        deaths = self.deaths
    }

    if self.ship then
        state.ship_state = self.ship:getState()
    end

    return state
end

function Player:setState(state, snap)
    self.ready_state = state.ready_state
    self.prefrence = state.preference
    self.name = state.name
    self.kills = state.kills
    self.deaths = state.deaths

    if not snap and self.respawn_time > state.respawn_time then
        self.respawn_time = self.respawn_time + 0.05*(state.respawn_time - self.respawn_time)
    else
        self.respawn_time = state.respawn_time
    end

    if self.uid ~= state.uid then
        log.error("Player IDs out of sync!")
    end

    self.eid = state.eid

    if self.ship and self.ship_eid ~= state.ship_eid then
        log.warn("Player ship eid out of date", self.ship, self.ship_eid)

        self.ship = self.world.entities[state.ship_eid]
        self.ship_eid = state.ship_eid
    end


    if state.ship_state and self.ship then
        self.ship:setState(state.ship_state, snap)
    end
end

return Player
