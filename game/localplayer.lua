local log = require "lib.log"
local inspect = require "lib.inspect"
local class = require "lib.middleclass"
local Vector = require "lib.hump.vector"

local const = require "game.const"
local Player = require "game.player"
local LocalPlayer = class("LocalPlayer", Player)

function LocalPlayer:initialize(world, uid, name, actual_eid)
    Player.initialize(self, world, uid, name, actual_eid)
end

function LocalPlayer:destroy()
    log.debug("localplayer destroy?")
    Player.destroy(self)
end

function LocalPlayer:draw()
    if self.ready_state == "respawn" and self.world.player_cam then
        local cx, cy = self.world.player_cam.cam:position()
        love.graphics.printf(string.format("%.2f", self.respawn_time), cx, cy, 200, "center", 0, 1, 1, 100, 0)
    end

    if self.ship then
        self.ship:draw(true)
        local s_pos = Vector(self.ship.body:getPosition())
        love.graphics.line(s_pos.x - 5, s_pos.y - 25, s_pos.x, s_pos.y - 20, s_pos.x + 5, s_pos.y - 25)
    end
end

function LocalPlayer:update(dt)
    Player.update(self, dt)
end

function LocalPlayer:getState()
    local state = Player.getState(self)
    return state
end

function LocalPlayer:setState(state, snap)
    Player.setState(self, state, snap)
end

return LocalPlayer
