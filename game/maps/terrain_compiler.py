from pprint import pprint
import argparse
import msgpack

import os.path

SCALE = 10

parser = argparse.ArgumentParser(description="Turns .OBJ terrain meshes into msgpack'd terrains")
parser.add_argument("obj", help="path to the .obj file")
parser.add_argument("-o","--output", nargs=1, help="output file")

args = parser.parse_args()

obj_lines = []
with open(args.obj, "r") as obj_file:
    obj_lines = [x.strip() for x in obj_file.readlines()]


terrain_meshes = {
    'verts': [],
    'view_faces': [],
    'phys_faces': [],
    'spawn_faces': []
}

current_object = None

vertices = []
faces = []
for line_idx, line in enumerate(obj_lines):
    if line.startswith("o "):
        obj_name = line[2:]
        if obj_name.startswith("terrain_view"):
            current_object = "view"
            terrain_meshes["view_faces"] = []
        elif obj_name.startswith("terrain_phys"):
            current_object = "phys"
            terrain_meshes["phys_faces"] = []
        elif obj_name.startswith("terrain_spawn"):
            current_object = "spawn"
            terrain_meshes["spawn_faces"] = []
        else:
            current_object = None

    elif line.startswith("v "):
        vert = [float(x)*SCALE for x in line[2:].split()]
        terrain_meshes["verts"].append(
            (vert[0],vert[2]) # ignore y axis
        )
    elif line.startswith("f"): # face definitions
        face_verts = line[2:].split()
        if current_object == "view":
            if len(face_verts) != 3:
                raise ValueError("Weapon view mesh faces can only be triangles")
        elif current_object == "phys":
            if len(face_verts) > 8 or len(face_verts) < 3:
                raise ValueError("Weapon phys mesh faces must have less than 8 verts")

        if current_object:
            face = []
            for vertex in face_verts: # individual vertex definitions
                v_def = [int(idx) for idx in vertex.split(sep="/") if idx]
                face.append(v_def[0])
            terrain_meshes[current_object + "_faces"].append(face)

pprint(terrain_meshes)

if not args.output:
    name,_ = os.path.splitext(args.obj)
    output_file = name + ".msgpack"
else:
    output_file = args.output

print("Writing compiled terrain meshes out to", output_file)

with open(output_file, "wb") as out_file:
    out_file.write(msgpack.packb(terrain_meshes))
