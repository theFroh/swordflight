local log = require "lib.log"
local inspect = require "lib.inspect"
local class = require "lib.middleclass"

local Timer = require "lib.hump.timer"
local Vector = require "lib.hump.vector"
local const = require "game.const"

local PlayerCamera = require "game.playercamera"
local World = require "game.world"
local Player = require "game.player"
local LocalPlayer = require "game.localplayer"
local Ship = require "game.ship"
local Sword = require "game.weapons.sword"

local ClientWorld = class("ClientWorld", World)

function ClientWorld:initialize(map_path, local_uid)
    World.initialize(self, map_path, true, false, local_uid) -- not authoritative

    self.player_cam = PlayerCamera()

    self.kill_feed_index = 0
    self.kill_feed_text = ""

    self.score_text = ""

    love.audio.setDistanceModel("exponentclamped")
end

function ClientWorld:sendInput(uid, input)
    World.sendInput(self, uid, input)
end

function ClientWorld:update(dt)
    local local_player = self:getPlayer(self.local_uid)
    if local_player and local_player.ship then
        -- self.player_cam.focus = Vector(local_player.ship.body:getPosition())
    end

    -- local aim_pos = Vector(self.player_cam.cam:worldCoords(love.mouse.getPosition()))

    -- self.player_cam:addEffector(0.25, aim_pos)

    self.player_cam:update(dt)
    World.update(self, dt)
end

function ClientWorld:setState(state)
    World.setState(self, state) -- snap
end

function ClientWorld:getState()
    return World.getState(self, state)
end

function ClientWorld:getDeltaState(last_full_state)
    World.getDeltaState(self, last_full_state)
end

function ClientWorld:applyDeltaState(delta_state)
    World.applyDeltaState(self, delta_state)

    local scores = {}
    for uid, eid in pairs(self.players) do
        local player = self:getPlayer(uid)
        table.insert(scores, {name = player.name, kills = player.kills, deaths = player.deaths, score = player.kills / math.max(1,player.deaths)})
    end

    table.sort(scores,
        function(a, b)
            return a.score < b.score
        end)

    local score_table = {}
    for _,player in ipairs(scores) do
        table.insert(score_table, string.format("%s %d:%d", player.name, player.kills, player.deaths))
    end

    self.score_text = table.concat(score_table, "\n")
end

function World:setPlayerState(uid, ship_state)
    local player = self:getPlayer(uid)
    if player and player.ship_eid then
        local ship = self.entities[player.ship_eid]
        ship:setState(ship_state)
    else
    end
end

function World:getPlayerState(uid)
    local state = nil
    local player = self:getPlayer(uid)
    if player and player.ship_eid then
        local ship = self.entities[player.ship_eid]
        state = ship:getState()
    end
    return state
end

-- we don't use the base draw, as we need to handle specific cases
function ClientWorld:draw()
    self.player_cam:attach()
    self.map:draw()

    for eid = 1, self.highest_eid do
        local entity = self.entities[eid]
        if entity then
            if entity.draw and not entity.parent then
                entity:draw()
            end
        end
    end
    self.player_cam:detach()

    love.graphics.printf(self.score_text, love.graphics.getWidth() - 10, 10, 200, "right", 0, 1, 1, 200, 0)
end

function ClientWorld:destroy()
    World.destroy(self)
end

return ClientWorld
