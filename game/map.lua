local log = require "lib.log"
local inspect = require "lib.inspect"
local class = require "lib.middleclass"
local MsgPack = require "lib.MessagePack"

local Vector = require "lib.hump.vector"

local Map = class("Map")

function Map:initialize(world, path)
    self.world = world
    self.body = love.physics.newBody(world.physworld, 0, 0, "static")
    self.body:setUserData(self)

    local meshes_file, _ = love.filesystem.read(path)
    local meshes = MsgPack.unpack(meshes_file)

    assert(meshes.view_faces and meshes.phys_faces, "Missing expected view and phys definitions.")

    -- load view verts into a mesh
    local view_vertices = {}
    for face_i, face in ipairs(meshes.view_faces) do
        table.insert(view_vertices, meshes.verts[face[1]])
        table.insert(view_vertices, meshes.verts[face[2]])
        table.insert(view_vertices, meshes.verts[face[3]])
    end
    self.mesh = love.graphics.newMesh(view_vertices, "triangles", "static")
    self.fixtures = {}

    -- load phys verts into a shape
    local phys_vertices = {}

    for face_i, face in ipairs(meshes.phys_faces) do
        local shape_vertices = {}
        for indice_i, indice in ipairs(face) do
            table.insert(shape_vertices, meshes.verts[indice][1])
            table.insert(shape_vertices, meshes.verts[indice][2])
        end
        local shape = love.physics.newPolygonShape(unpack(shape_vertices))
        local fixture = love.physics.newFixture(self.body, shape)
        -- TODO: USERDATA?

        table.insert(self.fixtures, fixture)
    end

    -- load spawn points
    self.spawn_points = {}
    for face_i, face in ipairs(meshes.spawn_faces) do
        local n_vertices = #face
        local spawn_point = Vector()
        for indice_i, indice in ipairs(face) do
            if not spawn_point then
                spawn_point = Vector(meshes.verts[indice][1], meshes.verts[indice][2])
            else
                spawn_point.x = spawn_point.x + (meshes.verts[indice][1] / n_vertices)
                spawn_point.y = spawn_point.y + (meshes.verts[indice][2] / n_vertices)
            end
        end

        -- spawn_point = self.body:localToWorld()

        table.insert(self.spawn_points, spawn_point)
    end
end


-- function Map:update(dt)
--
-- end

function Map:draw()
    love.graphics.setColor(0, 50, 110)
    local pos = Vector(self.body:getPosition())
    love.graphics.draw(self.mesh, pos.x, pos.y, self.body:getAngle())
    love.graphics.setColor(255,255,255)

    -- for _, point in ipairs(self.spawn_points) do
    --     love.graphics.circle("fill", point.x, point.y, 5)
    -- end
end

function Map:destroy()
    self.body:destroy()
end

return Map
