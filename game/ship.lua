local log = require "lib.log"
local inspect = require "lib.inspect"
local class = require "lib.middleclass"

local Timer = require "lib.hump.timer"
local Vector = require "lib.hump.vector"
local const = require "game.const"
local NetBody = require "game.netbody"
-- local Sword = require "game.weapons.sword"

local Ship = class("Ship", NetBody)
Ship.static.player_radius = 10
Ship.static.arm_length = 20

function Ship:initialize(world, x, y, parent, client_state)
    NetBody.initialize(self, world, x, y, client_state)
    self.body:setFixedRotation(true)

    self.health = 100
    self.smoothed_health = 100 -- for delta rendering
    self.invulnerable = false
    self.invulnerable_flash = false

    self.alive = true
    self.cleanup = 1

    self.shape = love.physics.newCircleShape(Ship.static.player_radius)
    self.fixture = love.physics.newFixture(self.body, self.shape)
    self.input = {
        -- aim = Vector(),
        move = Vector(),
        action = false
    }

    if not Ship.static.sprite then
        local canvas = love.graphics.newCanvas(Ship.static.player_radius * 2, Ship.static.player_radius * 2)
        love.graphics.setCanvas(canvas)
            love.graphics.clear()
            love.graphics.setBlendMode("alpha")
            love.graphics.setColor(0, 0, 0, 255)
            love.graphics.circle("fill", canvas:getWidth()/2, canvas:getHeight()/2, Ship.static.player_radius)
            love.graphics.setColor(255, 255, 255, 255)
            love.graphics.circle("line", canvas:getWidth()/2, canvas:getHeight()/2, Ship.static.player_radius)
        love.graphics.setCanvas()

        Ship.static.sprite = love.graphics.newImage(canvas:newImageData())
        Ship.static.sprite_width = Ship.static.sprite:getWidth()
        Ship.static.sprite_height = Ship.static.sprite:getHeight()
    end

    self.parent = parent
    self.parent_eid = parent and parent.eid or nil

    self.spinning = false

    self.action_power = 1

    -- self.sword = Sword(self.world, x, y)
    self.weapon = nil
    self.last_dealer = nil

    if self.world.is_fancy then
        self.death = love.audio.newSource("sound/death2.ogg", "static")
        self.death:setAttenuationDistances(30,2000)

        self.whoosh = love.audio.newSource("sound/rocket-roar.ogg", "static")
        -- self.whoosh:setRolloff(0.3)
        self.whoosh:setAttenuationDistances(20,1000)
        self.whoosh:setLooping(true)
        -- self.whoosh:play()

        self.respawn = love.audio.newSource("sound/respawn.ogg", "static")
        self.respawn:setAttenuationDistances(15,5000)
        self.respawn:setPosition(x,y)
        self.respawn:play()
        -- table.insert(self.sources, self.breezeblocks)
    end

    self.thrust = Vector()
end

function Ship:equipWeapon(weapon)
    self.weapon = weapon
    self.weapon:join()
end

function Ship:dealDamage(damage, dealer)
    if not self.invulnerable and self.alive then
        self:setHealth(self.health - damage)

        self.last_dealer = dealer or self.last_dealer
    end
end

function Ship:setHealth(new_health)
    self.health = math.max(0, new_health)

    if self.health == 0 then
        if self.world.is_authority then
            if self.parent then
                self.parent:shipDied()
            end

            self.parent = nil
            self.parent_eid = nil

            self.alive = false

            if self.weapon then
                self.weapon:release()
            end
        end
    end
end

function Ship:setInput(input)
    if input.move and input.move.x and input.move.y and type(input.move.x) == "number" and type(input.move.y) == "number" then
        self.input.move = Vector(input.move.x, input.move.y)
        self.input.move:trimInplace(1)
    end

    -- if input.aim and input.aim.x and input.aim.y and type(input.aim.x) == "number" and type(input.aim.y) == "number" then
    --     self.input.aim = Vector(input.aim.x, input.aim.y)
    -- end

    if input.action ~= self.input.action then
        -- self.spinning = not self.spinning
        self.input.action = input.action
    end

    -- if input.action ~= nil and type(input.action) == "boolean" then
    --     self.input.action = input.action
    -- end
end

-- TODO: http://www.iforce2d.net/b2dtut/constant-speed (impulses, maybe tie to thrusters?)

function Ship:update(dt)
    local move_thrust = self.input.move

    if self.spinning then
        local dir = -Vector(-self.weapon.body:getAngularVelocity(),0):rotateInplace(self.weapon.body:getAngle())
        dir:normalizeInplace()

        move_thrust = dir
        -- self:dealDamage(1)
    end

    local total_mass = self.weapon and self.body:getMass() + self.weapon.total_mass or self.body:getMass()
    local hover_thrust = Vector(0, -const.GRAVITY*total_mass*const.METER)

    local power = const.MAXVEL * const.TIGHTNESS


    if self.alive then
        self.thrust = -(power*move_thrust + hover_thrust - 0.7 * (power / const.MAXVEL * Vector(self.body:getLinearVelocity())))
        self.body:applyForce((power * move_thrust + hover_thrust - power / const.MAXVEL * Vector(self.body:getLinearVelocity())):unpack())
    end

    if self.weapon then
        self.weapon:update(dt)
    end

    if self.smoothed_health > self.health then
        self.smoothed_health = self.smoothed_health + (self.health - self.smoothed_health)*dt
    end

    if self.action_power < 1 then
        self.action_power = math.min(1,self.action_power + 2*dt)
    end


    if self.world.is_fancy then
        self.whoosh:setPosition(self.body:getPosition())
        self.whoosh:setVelocity(self.body:getLinearVelocity())

        local last_vol = self.whoosh:getVolume()
        local t_vol = math.min(1, self.thrust:len() / 150)

        self.whoosh:setVolume(last_vol + 10*dt*(t_vol - last_vol))

        -- local pow = math.max(0.1,math.min(1.5,Vector(self.body:getLinearVelocity()):len() / const.MAXVEL))
        -- log.debug("pow", pow)
        -- local last_vol = self.whoosh:getVolume()
        -- local t_vol = 2*math.min(1, pow - 0.2)
        -- self.whoosh:setVolume(last_vol + 10*dt*(t_vol - last_vol))
        --
        -- local last_pitch = self.whoosh:getPitch()
        -- local t_pitch = pow*5
        -- self.whoosh:setPitch(last_pitch + 20*dt*(t_pitch - last_pitch))
    end

    if self.world.is_authority then
        self.weapon:setHold(self.input.action)
    end

    -- if self.input.action then
    --     -- self.weapon.holding = true
    -- else
    --     -- self.weapon.holding = false
    -- end

    if not self.alive then
        self.cleanup = self.cleanup - 0.2*dt

        if self.cleanup <= 0 and self.world.is_authority then
            self:destroy()
        end
    end
end

function Ship:destroy()
    NetBody.destroy(self)
    if self.weapon then
        self.weapon:destroy()
    end
end

function Ship:draw(is_local)
    if is_local then
        love.graphics.setColor(255, 79, 103)
    elseif self.alive then
        love.graphics.setColor(255,255,255)
    else
        love.graphics.setColor(235,235,235, self.cleanup*255)
    end

    if self.weapon then
        self.weapon:draw()
    end

    local p = Vector(self.body:getPosition())
    local v = Vector(self.body:getLinearVelocity())

    local radius = Vector(Ship.static.player_radius, Ship.static.player_radius) + v

    -- local old_width = love.graphics.getLineWidth()
    -- love.graphics.setLineWidth(Ship.static.player_radius*2)
    -- love.graphics.line(p.x, p.y, (p - 0.05*v):unpack())
    -- love.graphics.setLineWidth(old_width)

    local r,g,b,a = love.graphics.getColor()
    love.graphics.setColor(0,0,0,255)
    love.graphics.circle("fill", p.x, p.y, Ship.static.player_radius, 18)

    if not self.invulnerable_flash then
        if is_local then
            love.graphics.setColor(255,255,255)
        else
            love.graphics.setColor(255,0,0)
        end
        love.graphics.circle("fill", p.x, p.y, self.smoothed_health/100*Ship.static.player_radius, 18)
        love.graphics.setColor(r,g,b,a)
        love.graphics.circle("fill", p.x, p.y, self.health/100*Ship.static.player_radius, 18)
    else
        love.graphics.setColor(r,g,b,a)
    end

    love.graphics.circle("line", p.x, p.y, Ship.static.player_radius, 18)

    if self.world.is_fancy and self.alive then
        local num_thrusters = 5
        for i = 1, num_thrusters do
            -- thrust in each control direction
            local a = math.pi * (i/(num_thrusters))
            local t_v = self.thrust:projectOn(Vector(1,0):rotated(a)) * love.math.randomNormal(0.1, 1)

            love.graphics.line(p.x,p.y, p.x + t_v.x, p.y + t_v.y)
        end
    end

    -- love.graphics.draw(Ship.static.sprite, p.x, p.y, 0, 1, 1, Ship.static.sprite_width/2, Ship.static.sprite_height/2)
    love.graphics.setColor(255,255,255,255)
    -- love.graphics.line(p.x, p.y, (p + Vector(Ship.static.player_radius):rotateInplace(self.body:getAngle())):unpack())
end

function Ship:setState(state, snap)
    NetBody.setState(self, state, snap)
    self.input = {
        action = state.input.action,
        move = Vector(state.input.move.x, state.input.move.y),
        -- aim = Vector(state.input.aim.x, state.input.aim.y),
    }
    self:setHealth(state.health)
    self.parent_eid = state.parent_eid
    self.spinning = state.spinning

    if not state.alive and self.alive then
        self.parent = nil
        if self.weapon then
            self.weapon:release()
        end

        if self.world.is_fancy then
            self.death:setPosition(self.body:getX(), self.body:getY())
            self.death:play()
        end
    end

    self.alive = state.alive
    self.cleanup = state.cleanup


    if self.weapon and state.weapon then
        self.weapon:setState(state.weapon, snap)
    end
end

function Ship:getState()
    local state = NetBody.getState(self)

    state.input = {
        action = self.input.action,
        move = {
            x = self.input.move.x,
            y = self.input.move.y
        },
        -- aim = {
        --     x = self.input.aim.x,
        --     y = self.input.aim.y
        -- }
    }
    state.parent_eid = self.parent_eid
    state.health = self.health
    state.spinning = self.spinning
    state.alive = self.alive
    state.cleanup = self.cleanup

    if self.weapon then
        state.weapon = self.weapon:getState()
    end

    return state
end

return Ship
