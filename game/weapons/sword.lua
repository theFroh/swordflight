local log = require "lib.log"
local inspect = require "lib.inspect"
local class = require "lib.middleclass"
local easing = require "lib.easing"
local MsgPack = require "lib.MessagePack"

local Vector = require "lib.hump.vector"
local const = require "game.const"
local NetBody = require "game.netbody"

local Sword = class("Sword", NetBody)

-- TODO: Once weapon archetype is complete, generalise into a generic weapon interface

function Sword:initialize(world, parent, client_state)
    NetBody.initialize(self, world, parent.body:getX(), parent.body:getY(), client_state)

    self:loadResources()

    self.parent = parent
    self.parent_eid = parent.eid
    self.joined = nil

    self.total_mass = self.body:getMass()

    -- gameplay variables
    self.power = 0 -- should range from 0 to 1
    self.last_power = 0
    self.attacking = false
    self.holding = false

    self.hold_ang = 0

    self.impacts = {}
    self.trail = {}

    self.smooth_angvel = 0

    self.damage = {
        weak = 10,
        normal = 35,
        strong = 50
    }

    self.impact_debounce = {} -- ignore rapid collision events
    self.sparks = {}

    self.tip_pos = Vector(self.body:getWorldPoint(0, self.length))
    self.tip_vel = Vector()

    if self.world.is_fancy then
        self.solo_silenced = false -- to avoid double playbacks

        self.sources = {}
        self.hit_ground = love.audio.newSource("sound/hit_ground.ogg", "static")
        self.hit_ground:setAttenuationDistances(10,1200)

        self.clang = love.audio.newSource("sound/clang.ogg", "static")
        self.clang:setAttenuationDistances(20,1200)


        self.clink = love.audio.newSource("sound/clink2.ogg", "static")
        self.clink:setAttenuationDistances(20,1200)

        self.hit_player = love.audio.newSource("sound/ouch.ogg", "static")
        self.hit_player:setAttenuationDistances(25,2000)

        self.whoosh = love.audio.newSource("sound/wind.ogg", "static")
        self.whoosh:setLooping(true)
        self.whoosh:setPitch(1.3)
        self.whoosh:setAttenuationDistances(25,800)

        self.mclunk = love.audio.newSource("sound/mclunk.ogg", "static")
        self.mclunk:setAttenuationDistances(5,800)

        table.insert(self.sources, self.hit_ground)
        table.insert(self.sources, self.clang)
    end

    -- self.angle_target = 2*math.pi*love.math.random()
    -- self.body:setAngularDamping(0.1)
end

function Sword:setHold(hold)
    if hold then
        if not self.holding then
            self.holding = true
            self.hold_ang = self.joint:getJointAngle()
            -- self.hold_ang = self.body:getJointAngle()
        end
    else
        self.holding = false
    end
end

function Sword:handlePreSolve(own_fixture, other_fixture, coll)
    local other_object = other_fixture:getBody():getUserData()

    if other_object then
        local cx,cy, _,_ = coll:getPositions()
        local impact_point = Vector(self.body:getLocalPoint(cx, cy))

        local is_sharp = false
        for _,aabb in ipairs(self.sharp_aabbs) do
            if impact_point.x >= aabb.x and impact_point.x < (aabb.x + aabb.w) and
                impact_point.y >= aabb.y and impact_point.y < (aabb.y + aabb.h) then

                is_sharp = true
                break
            end
        end

        table.insert(self.impacts, {
                pos = impact_point,
                sharp = is_sharp
            }
        )

        if not self.impact_debounce[other_object] then
            local weapon_vel = Vector(self.body:getLinearVelocityFromWorldPoint(cx, cy))
            local target_vel = Vector(other_fixture:getBody():getLinearVelocityFromWorldPoint(cx, cy))

            local impact_norm = Vector(coll:getNormal())
            local impact_vel = target_vel - weapon_vel

            local approach_vel = impact_vel:projectOn(impact_norm):len()

            local strength = nil
            if approach_vel < 200 then
                strength = nil
            elseif approach_vel < 500 then
                strength = "weak"
            elseif approach_vel < 700 then
                strength = "normal"
            else
                strength = "strong"
            end

            if self.world.is_authority then
                if self.attacking then
                    if other_object.class.name == "Ship" and strength then
                        log.debug("Hit Ship", approach_vel, strength)
                        other_object:dealDamage(self.damage[strength], self.parent.parent)
                    end
                end
            end

            if self.world.is_fancy and approach_vel > 100 then
                log.debug("HITTER",approach_vel)

                if is_sharp then
                    if not self.solo_silenced then
                        if other_object.class.name == "Sword" and self.attacking then
                            other_object.solo_silenced = true

                            if other_object.attacking then
                                self.clink:setPosition(cx,cy)
                                self.clink:setPitch(love.math.randomNormal(0.1, 1))
                                self.clink:play()
                            else
                                self.clang:setPosition(cx,cy)
                                self.clang:setPitch(love.math.randomNormal(0.1, 1))
                                self.hit_ground:setVolume(math.min(1,approach_vel / 700))
                                self.clang:play()
                            end
                        else
                            if self.attacking and other_object.class.name == "Ship" then
                                self.hit_player:setPosition(cx,cy)
                                self.hit_player:setPitch(love.math.randomNormal(0.1, 1))
                                self.hit_ground:setVolume(math.min(1,approach_vel / 700))
                                self.hit_player:play()
                            else
                                self.hit_ground:setPosition(cx,cy)
                                self.hit_ground:setVolume(math.min(1,approach_vel / 700))
                                self.hit_ground:setPitch(love.math.randomNormal(0.1, 1))
                                self.hit_ground:play()
                            end
                        end
                    end

                    local n = love.math.random(1, math.min(100, math.floor(approach_vel / 10)))
                    for i = 1, n do
                        local dir = impact_vel:mirrorOn(impact_norm)
                        local rot = love.math.randomNormal(0.4, 0)
                        dir:rotateInplace(rot)

                        table.insert(self.sparks, {
                            pos = Vector(cx, cy),
                            normal = impact_norm,
                            vel = love.math.random() * dir,
                            lifetime = love.math.random(),
                            life = 1
                        })
                    end

                    -- If we sort now, removing later will not require shuffles
                    table.sort(self.sparks,
                        function(a,b)
                            return a.life < b.life
                        end
                    )
                end
                self.solo_silenced = false
            end

            self.impact_debounce[other_object] = true
        end
    end
end

function Sword:handlePostSolve(own_fixture, other_fixture, coll, normalimpulse1, tangentimpulse1, normalimpulse2, tangentimpulse2)

end


function Sword:join()
    local anchor = Vector(self.parent.body:getPosition())
    self.body:setPosition(anchor.x, anchor.y)
    self.body:setAngle(0)

    local joint = Vector(self.body:getPosition())
    self.joint = love.physics.newRevoluteJoint(self.parent.body, self.body, joint.x, joint.y, false)
    self.joint:setMaxMotorTorque(self.body:getInertia()*35)

end

function Sword:release()
    self.joint:destroy()
    self.joint = nil
end

function normalizeAngle(ang)
    while ang <= -math.pi do ang = ang + 2*math.pi end
    while ang > math.pi do ang = ang - 2*math.pi end
    return ang
end

function Sword:update(dt)
    self.tip_pos = Vector(self.body:getWorldPoint(0, self.length))
    self.tip_vel = Vector(self.body:getLinearVelocityFromLocalPoint(0, self.length))

    local t_angvel = self.attacking and self.body:getAngularVelocity() or 0
    self.smooth_angvel = self.smooth_angvel + 10*dt*(t_angvel - self.smooth_angvel)

    -- the sword relies on being swung to trigger an attack intent
    self.last_power = self.power
    self.power = math.abs(self.body:getAngularVelocity())/12
    self.impact_debounce = {}

    -- log.debug(self.joint:isMotorEnabled())
    if self.joint then
        if self.holding then
            if self.joint and not self.joint:isMotorEnabled() then
                self.joint:setMotorEnabled(true)
                if self.world.is_fancy then
                    self.mclunk:setPosition(self.body:getPosition())
                    self.mclunk:play()
                end
            end
            local ang_vel = self.joint:getJointSpeed()
            local diff = self.hold_ang - self.joint:getJointAngle()

            self.joint:setMotorSpeed((10*diff - 2*ang_vel))
        else
            self.joint:setMotorEnabled(false)
            self.joint:setMotorSpeed(0)
        end
    end

    if not self.holding and not self.attacking and self.power > 0.65 then
        self.attacking = true
        -- self.body:setAngularDamping(0)
    elseif self.power < 0.4 then
        self.attacking = false
        -- self.body:setAngularDamping(5)
    end

    if self.world.is_fancy then
        if self.attacking then
            log.debug(self.power, (self.power - self.last_power) / dt)
        end
        if self.attacking and math.abs(self.power) > 0.8 then
            -- log.debug(math.max(0,(self.power - self.last_power) / dt))
            self.whoosh:setPosition(self.tip_pos:unpack())
            self.whoosh:setVelocity(self.tip_vel:unpack())
            if not self.whoosh:isPlaying() then
                self.whoosh:play()
            end
            -- self.whoosh:setVolume(math.min(1, self.power - 0.7))

            local last_vol = self.whoosh:getVolume()
            local t_vol = 2*math.min(1, self.power - 0.9)
            self.whoosh:setVolume(last_vol + 10*dt*(t_vol - last_vol))

            -- local last_pitch = self.whoosh:getPitch()
            -- local t_pitch = self.power*3.5
            -- self.whoosh:setPitch(last_pitch + 20*dt*(t_pitch - last_pitch))
        else
            local new_vol = self.whoosh:getVolume() - 1.4*dt
            if new_vol < 0 then
                self.whoosh:stop()
                self.whoosh:setVolume(0)
            else
                self.whoosh:setVolume(new_vol)
            end
        end

        for i = #self.sparks, 1, -1 do
            local spark = self.sparks[i]

            if spark.life <= 0 then
                table.remove(self.sparks, i)
            else
                spark.vel = spark.vel + Vector(0,const.GRAVITY * const.METER)*dt
                spark.pos = spark.pos + spark.vel * dt
                spark.life = (spark.lifetime * spark.life - dt) / spark.lifetime
            end
        end
    end
end

function Sword:destroy()
    NetBody.destroy(self)
end

function Sword:action(dt)
end

function Sword:draw()
    local r,g,b,a = love.graphics.getColor()
    local pos = Vector(self.body:getPosition())

    local curr_ang = self.body:getAngle()
    local blend_ang = self.last_ang

    if self.attacking then
        love.graphics.setColor(
            math.min(255,r),
            math.min(255,g*0.7),
            math.min(255,b*0.7),
            255
        )
    elseif self.holding then
        love.graphics.setColor(r, g*0.7, b)
    end
    -- draw a little trail!

    love.graphics.arc("fill", pos.x, pos.y, self.length, curr_ang + math.pi/2,
        curr_ang + math.pi/2 - 0.03*self.smooth_angvel
    )


    love.graphics.draw(self.mesh, pos.x, pos.y, self.body:getAngle())

    if self.world.is_fancy then
        love.graphics.setColor(255,255,255)
        for _,spark in ipairs(self.sparks) do
            love.graphics.line(spark.pos.x, spark.pos.y, spark.pos.x + (spark.vel.x/50)*spark.life, spark.pos.y + (spark.vel.y/50)*spark.life)
        end
    end

    love.graphics.setColor(r,g,b,a)
end

function Sword:setState(state, snap)
    NetBody.setState(self, state, snap)
    self.parent_eid = state.parent_eid
    self.holding = state.holding
    self.hold_ang = state.hold_ang
end

function Sword:getState()
    local state = NetBody.getState(self)
    state.parent_eid = self.parent_eid
    state.holding = self.holding
    state.hold_ang = self.hold_ang
    return state
end

-- TODO: Should reuse this, make it static? Load once?
function Sword:loadResources()
    local meshes_file, _ = love.filesystem.read("game/weapons/obj/sword.msgpack")
    local meshes = MsgPack.unpack(meshes_file)

    assert(meshes.view_faces and meshes.phys_faces, "Missing expected view and phys definitions.")

    -- origin to tip
    self.length = 0

    -- load view verts into a mesh
    local view_vertices = {}
    for face_i, face in ipairs(meshes.view_faces) do
        local v1,v2,v3 = meshes.verts[face[1]], meshes.verts[face[2]], meshes.verts[face[3]]
        table.insert(view_vertices, v1)
        table.insert(view_vertices, v2)
        table.insert(view_vertices, v3)

        self.length = math.max(self.length, v1[2], v2[2], v3[2])
    end
    self.mesh = love.graphics.newMesh(view_vertices, "triangles", "static")
    self.fixtures = {}

    -- load phys verts into a shape
    local phys_vertices = {}
    local density = .5
    local restitution = .3

    for face_i, face in ipairs(meshes.phys_faces) do
        local shape_vertices = {}
        for indice_i, indice in ipairs(face) do
            table.insert(shape_vertices, meshes.verts[indice][1])
            table.insert(shape_vertices, meshes.verts[indice][2])
        end
        local shape = love.physics.newPolygonShape(unpack(shape_vertices))
        local fixture = love.physics.newFixture(self.body, shape, density)
        fixture:setRestitution(restitution)
        table.insert(self.fixtures, fixture)
    end

    -- load sharp AABB region/s
    self.sharp_aabbs = {}
    for face_i, face in ipairs(meshes.sharp_faces) do
        local aabb = {}
        local min, max = Vector(), Vector()
        for indice_i, indice in ipairs(face) do
            local x,y = meshes.verts[indice][1], meshes.verts[indice][2]
            min.x = math.min(min.x, x)
            min.y = math.min(min.y, y)
            max.x = math.max(max.x, x)
            max.y = math.max(max.y, y)
        end

        aabb.x = min.x
        aabb.y = min.y
        aabb.w = max.x - min.x
        aabb.h = max.y - min.y

        table.insert(self.sharp_aabbs, aabb)
    end

    self.mesh = love.graphics.newMesh(view_vertices, "triangles", "static")
end

return Sword
