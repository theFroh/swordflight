from pprint import pprint
import argparse
import msgpack

import os.path

SCALE = 10

parser = argparse.ArgumentParser(description="Turns .OBJ weapons into msgpack'd weapons")
parser.add_argument("obj", help="path to the .obj file")
parser.add_argument("-o","--output", nargs=1, help="output file")

args = parser.parse_args()

obj_lines = []
with open(args.obj, "r") as obj_file:
    obj_lines = [x.strip() for x in obj_file.readlines()]


weapon_meshes = {
    'verts': [],
    'view_faces': [],
    'phys_faces': [],
    'sharp_faces': []
}

current_object = None

vertices = []
faces = []
for line_idx, line in enumerate(obj_lines):
    if line.startswith("o "):
        obj_name = line[2:]
        if obj_name.startswith("wep_view"):
            current_object = "view"
            weapon_meshes["view_faces"] = []
        elif obj_name.startswith("wep_phys"):
            current_object = "phys"
            weapon_meshes["phys_faces"] = []
        elif obj_name.startswith("wep_sharp"):
            current_object = "sharp"
            weapon_meshes["sharp_faces"] = []
        else:
            current_object = None

    elif line.startswith("v "):
        vert = [float(x)*SCALE for x in line[2:].split()]
        weapon_meshes["verts"].append(
            (vert[0],vert[2]) # ignore y axis
        )
    elif line.startswith("f"): # face definitions
        face_verts = line[2:].split()
        if current_object == "view":
            if len(face_verts) != 3:
                raise ValueError("Weapon view mesh faces can only be triangles")
        elif current_object == "phys":
            if len(face_verts) > 8 or len(face_verts) < 3:
                raise ValueError("Weapon phys mesh faces must have less than 8 verts")
        elif current_object == "sharp":
            if len(face_verts) != 4:
                print("Warning! Weapon damage hurtboxes should be AABB's, so 4 verts (keeping them straight best represents ingame, too)")

        if current_object:
            face = []
            for vertex in face_verts: # individual vertex definitions
                v_def = [int(idx) for idx in vertex.split(sep="/") if idx]
                face.append(v_def[0])
            weapon_meshes[current_object + "_faces"].append(face)

pprint(weapon_meshes)

if not args.output:
    name,_ = os.path.splitext(args.obj)
    output_file = name + ".msgpack"
else:
    output_file = args.output

print("Writing compiled weapon meshes out to", output_file)

with open(output_file, "wb") as out_file:
    out_file.write(msgpack.packb(weapon_meshes))
