local log = require "lib.log"
local inspect = require "lib.inspect"
local class = require "lib.middleclass"
local Vector = require "lib.hump.vector"

local NetBody = class("NetBody")

function NetBody:initialize(world, x, y, client_state)
    self.predicted = false -- Should this body expect to be checked for validity
    self.world = world
    self.body = love.physics.newBody(world.physworld, x, y, "dynamic")
    self.body:setUserData(self)

    if world.is_authority then
        self.eid = self.world:register(self)
        log.debug("On Authority")
    else
        assert(client_state, "Missing required state for initialization on client")
        -- self.eid = client_state.eid
        self:setState(client_state, true)
        self.world.entities[self.eid] = self
    end
end

function NetBody:destroy()
    if self.world.is_authority then
        self.world:deregister(self.eid)
    else
        self.world.entities[self.eid] = false
    end

    self.body:destroy()
end

-- TODO: complete deltas
function NetBody:getDeltaState(old)
    local curr = self:getState()
    -- Within an entity, attributes can only change, not be removed or added
    local delta = {
        class = "NetBody",
        delta = true,
    }

    -- if curr.pos.x ~= old.pos.x then
    --     delta.pos.x ~= old.pos.x
end

function NetBody:getState()
    local x,y = self.body:getPosition()
    local dx,dy = self.body:getLinearVelocity()

    local state = {
        class = self.class.name,
        pos = {
            x = x,
            y = y
        },

        vel = {
            x = dx,
            y = dy
        },

        ang = self.body:getAngle(),
        ang_vel = self.body:getAngularVelocity(),

        eid = self.eid
    }

    if self.predicted then
        -- do we need to do anything special here?
    end

    return state
end

function NetBody:setState(state, snap)
    local pos = Vector(self.body:getPosition())
    local ang = self.body:getAngle()
    local pos_err = pos:dist(Vector(state.pos.x, state.pos.y))
    local ang_err = state.ang - ang

    local state_pos = Vector(state.pos.x, state.pos.y)
    -- TODO: move to config

    -- if self.predicted then -- If we're predicted, we'll need to know EXACTLY where we were @ tick blah, not where we were smoothed to
    --     self.real.pos =
    -- end

    if snap or pos_err > 100 then
        self.body:setPosition(state_pos:unpack())
        log.warn("Snapping position.")
    else
        self.body:setPosition((pos + 0.1 * (state_pos - pos)):unpack())
    end

    if snap or math.abs(ang_err) > math.pi/2 then
        self.body:setAngle(state.ang)
        log.warn("Snapping angle.")
    else
        self.body:setAngle(ang + ang_err*0.1)
    end

    -- Don't have to smooth derivatives
    self.body:setLinearVelocity(state.vel.x, state.vel.y)
    self.body:setAngularVelocity(state.ang_vel)

    self.eid = state.eid
end

-- TODO: expand this to do deltas?
function NetBody.static.compareState(a, b)
    -- TODO: use epsilon etc?
    local within_bounds = false
    if a.pos.x == b.pos.x and a.pos.y == b.pos.y and
        a.vel.x == b.vel.x and a.vel.y == b.vel.y and
        a.ang == b.ang and a.ang_vel == b.ang_vel then
        within_bounds = true
    end
    return within_bounds
end

return NetBody
