local log = require "lib.log"
local inspect = require "lib.inspect"
local class = require "lib.middleclass"

local Vector = require "lib.hump.vector"
local Timer = require "lib.hump.timer"
local const = require "game.const"

local Sparks = class("Sparks")

function Sparks:initialize(x,y, rot, power)

end

function Sparks:update(dt)

end

function Sparks:draw()

end

return Sparks
