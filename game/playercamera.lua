local log = require "lib.log"
local inspect = require "lib.inspect"
local class = require "lib.middleclass"

local Camera = require "lib.hump.camera"
local Vector = require "lib.hump.vector"

local PlayerCamera = class("PlayerCamera")

function PlayerCamera:initialize(focus)
    self.active = false -- not active, sit
    self.focus = focus or Vector()
    self.effectors = {} -- contain {weight = [-1,1], location = Vector(x,y), easeoff = [0, distance]}

    self.cam = Camera(self.focus.x, self.focus.y)
    self.cam.smoother = Camera.smooth.damped(3)

    self.max_drift = 500
    self.window_radius = 50

    self.last_pos = Vector()
    self.world_pos = Vector()
end


function PlayerCamera:addEffector(weight, pos, easeoff)
    table.insert(self.effectors, {weight = weight, pos = pos, easeoff = easeoff})
end

function PlayerCamera:update(dt)
    local aim = self.focus

    for i = #self.effectors, 1, -1 do
        local effector = self.effectors[i]
        local d = effector.pos:dist(self.focus)

        aim = aim + effector.weight * (aim - effector.pos) -- TODO: Add smooth fall off range (smoothly reduce effect)

        table.remove(self.effectors, i)
    end

    -- ensure focus stays on screen
    local diff = self.focus - aim
    diff:trimInplace(self.max_drift)
    aim = self.focus + diff

    self.cam:lockWindow(aim.x, aim.y,
        love.graphics.getWidth()/2 - self.window_radius, love.graphics.getWidth()/2 + self.window_radius,
        love.graphics.getHeight()/2 - self.window_radius, love.graphics.getHeight()/2 + self.window_radius)


    local wx, wy = self.cam:position()
    local vel = Vector(wx - self.world_pos.x, wy - self.world_pos.y) / dt

    self.world_pos.x = wx
    self.world_pos.y = wy

    love.audio.setVelocity(vel:unpack())
    love.audio.setPosition(wx, wy, 200)
end

function PlayerCamera:attach()
    self.cam:attach()
end

function PlayerCamera:detach()
    self.cam:detach()
end

return PlayerCamera
