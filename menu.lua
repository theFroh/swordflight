local menu = {} -- gamestate
local Gamestate = require "lib.hump.gamestate"
local log = require "lib.log"
local inspect = require "lib.inspect"
local suit = require "lib.suit"

local fonts = require "fonts"
local game = require "game"

local tPx, fPx = love.window.toPixels, love.window.fromPixels
local middleWidth = 400

function menu:init()
    love.keyboard.setKeyRepeat(true)
end

local name_input = {text = "Nobody"}
local addr_input = {text = "localhost"}

local error_color = {fg = {255, 255, 255}, bg = {185, 54, 54}}

function menu:update(dt)
    suit.layout:reset(0,0)
    suit.layout:row(tPx(love.graphics.getWidth(), tPx(40)))

    suit.Label("Swordflight", {font = fonts.title}, suit.layout:row(nil, tPx(90)))
    suit.Label("A game sword with flight. Sword.", {font = fonts.text}, suit.layout:row(nil, tPx(20)))

    suit.layout:reset(tPx(love.graphics.getWidth() / 2 - middleWidth / 2), tPx(200))
    suit.layout:push(suit.layout:row(middleWidth, tPx(40)))
        suit.layout:padding(tPx(10),0)
        suit.Label("Username:", {align="right", font = fonts.bold_text}, suit.layout:col(tPx(middleWidth / 3), tPx(40)))
        suit.Input(name_input, {font = fonts.text}, suit.layout:col(tPx(middleWidth * 2/3 - 10)))
    suit.layout:pop()

    suit.layout:row(middleWidth, tPx(20))
    suit.Label("If you want to join an existing server", {font = fonts.text}, suit.layout:row(middleWidth, tPx(40)))
    suit.layout:push(suit.layout:row(middleWidth, tPx(40)))
        suit.layout:padding(tPx(10),0)
        suit.Label("Address:", {align="right", font = fonts.bold_text}, suit.layout:col(tPx(middleWidth / 3), tPx(40)))
        local addr = suit.Input(addr_input, {font = fonts.text}, suit.layout:col(tPx(middleWidth * 1.5/3 - 10 * 2)))
        local join = suit.Button("Join", {font = fonts.text}, suit.layout:col(tPx(middleWidth * 0.5/3)))
    suit.layout:pop()

    suit.layout:row(middleWidth, tPx(20))
    suit.Label("Or to host a listen server", {font = fonts.text}, suit.layout:row(middleWidth, tPx(40)))
    suit.layout:push(suit.layout:row(middleWidth, tPx(40)))
        suit.layout:col(middleWidth/3, tPx(40))
        local host = suit.Button("Host", {font = fonts.text}, suit.layout:col())
    suit.layout:pop()

    if join.hit then
        Gamestate.switch(game, name_input.text, addr_input.text, false)
    elseif host.hit then
        Gamestate.switch(game, name_input.text, nil, true)
    end
end

function menu:draw()
    suit.draw()
end

function menu:textinput(t)
    suit.textinput(t)
end

function menu:keypressed(key)
    log.debug("PASTE", key)

    if key == "v" and love.keyboard.isDown("lctrl", "rctrl") then
        log.debug(addr_input.id, inspect(addr_input))
        if suit.isHovered(addr_input.id) then
            addr_input.text = addr_input.text .. love.system.getClipboardText()
        end
    end

    suit.keypressed(key)
end

return menu
